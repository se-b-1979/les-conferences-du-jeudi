package panel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;

//importation 
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Database.pojo.Conference;
import model.ConferenceModel;

//creation de la classe Home
public class Home extends JPanel  {
	public Home() {
		//creation du label
		JLabel lab = new JLabel("Bienvenue sur l�application.");
		//ajout du label
		this.add(lab);
		//creation du bouton
		JButton btn = new JButton("Clic et.....Surprise");
		//ajout du bouton
		this.add(btn);
		 Calendar date = Calendar.getInstance();
			//creation du tableau
			Conference conf1 = new Conference("Mr beguin",1,"premiere conference",date,1,1,1);
			Conference conf2 = new Conference("Mr Jacob",1,"Deuxieme conference",date,1,1,1);
			Conference conf3 = new Conference("Mlle Checa",1,"Troisieme conference",date,1,1,1);
			
			ArrayList<Conference> tableau= new ArrayList<>();
			//tableaux
			tableau.add(conf1);
			tableau.add(conf2);
			tableau.add(conf3);
			ConferenceModel cm = new ConferenceModel(tableau);
			JTable table = new JTable(cm);
			JScrollPane scroll = new JScrollPane(table);
			this.add(scroll);


	}
}

