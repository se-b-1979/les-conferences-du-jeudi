package Database.pojo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
*
* @author user
*/
public class Conference {
//declaration de la classe
	private String nomConferencier;
	private int idConference;
	private String titreConference;
	private Calendar dateConference;
	private int idConferencier;
	private int idSalle;
	private int idTheme;
	
    //constructeur
    public Conference(){
        
    }
    /**
     * 
     * @param _nomConferencier
     * @param _idConference
     * @param _titreConference
     * @param _dateConference
     * @param _idConferencier
     * @param _idSalle
     * @param _idTheme
     */

    
    public Conference(String _nomConferencier,int _idConference, String _titreConference, Calendar _dateConference, int _idConferencier, int _idSalle, int _idTheme){
        this.nomConferencier = _nomConferencier;
    	this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.dateConference = _dateConference;
        this.idConferencier = _idConferencier;
        this.idSalle = _idSalle;
        this.idTheme = _idTheme;
    }
    
    //getter et setter
    public String getnomConferencier() {
    	return this.nomConferencier;
    }
    public void setnomConferencier(String _nomConferencier) {
    	this.nomConferencier = _nomConferencier;
    }
    public int getidConference(){
        return this.idConference;
    }
    public void setidConference(int  _idConference){
        this.idConference = _idConference;
    }
    public String gettitreConference(){
        return this.titreConference;
    }
    public void settitreConference(String  _titreConference){
        this.titreConference = _titreConference;
    }
    public Calendar getdateConference(){
        return this.dateConference;
    }
    public void setdateConference(Calendar  _dateConference){
        this.dateConference = _dateConference;
    }
    public int getidConferencier(){
        return this.idConferencier;
    }
    public void setidConferencier(int  _idConferencier){
        this.idConferencier = _idConferencier;
    }
    public int getidSalle(){
        return this.idSalle;
    }
    public void setidSalle(int  _idSalle){
        this.idSalle = _idSalle;
    }
    public int getidTheme(){
        return this.idTheme;
    }
    public void setidTheme(int  _idTheme){
        this.idTheme = _idTheme;
    }
    //methode calendar
    public String getDateString() {
    	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    	return format.format(this.getdateConference().getTimeInMillis());
	}
    


}
