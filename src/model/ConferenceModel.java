package model;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import Database.pojo.Conference;
public class ConferenceModel extends AbstractTableModel 
{
	//creation arraylist et tableau
	private ArrayList <Conference> tableau = new ArrayList<>();
	private final String[] columnsName = {"Titre","Date","Conferencier"};
//constructeur du tableau
	 public ConferenceModel(ArrayList<Conference> tableau){
	      this.tableau = tableau;  
	      
	      //implementation des methodes
	    }
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnsName.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return tableau.size();
	}
	
	@Override
	public String getColumnName(int index) {
		// TODO Auto-generated method stub
		//retour des nom de colonne via l index
		return columnsName[index] ;
	}
	
//recuperation des valeurs du tableau
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		Conference c = tableau.get(rowIndex);
		switch(columnIndex) {
		case 0:
			return c.gettitreConference();
		case 1 :
			return c.getDateString();
		case 2 : 
			return c.getnomConferencier();
			default : 
				return "";	
		}
	}

}
